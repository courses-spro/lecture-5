# Lecture 5. Modules and classes

### Useful Links:

https://nodejs.org/api/modules.html

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes

## Homework:
Create a tic-tac-toe game. Should be run within console. ( To clear console after each step use console.clear )
Write the game using classes. Each class should be in separate file. All literals should be in constants. For storing constants use directory
./src/constants
Game should be launched by command node ./src/game/index.js
It should not throw any errors during execution (make type checking during inputs)
