class Cell {
  value;

  setValue() {

  }
}

const PRIVATE_METHOD = Symbol('asd');

class Board {
  matrix = [[Cell, Cell, Cell], [], []]
  player1 = new User();
  player2 = new User();



  // private methods case
  accessToPrivateMethod() {
    this[PRIVATE_METHOD]()
  }

  static someStaticField = 123;

  [PRIVATE_METHOD]() {


  }
  // end

}

class Leaderboard {

}

class User {

}
