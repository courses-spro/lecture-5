const Circle = require('./Shapes/circle');

const Rectangle = require('./Shapes/rectangle');

const arr = [
  new Circle(5),
  new Circle(7),
  new Rectangle(3),
  new Rectangle(9)
];

const maps = arr.map(it  => it.area);

console.log(maps);
