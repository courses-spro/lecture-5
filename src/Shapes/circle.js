const Shape = require('./index');

class Circle extends Shape {
  constructor(radius) {
    super();
    this.radius = radius;
  }


  get area() {
    return Math.PI * this.radius*2;
  }
}


module.exports = Circle;
