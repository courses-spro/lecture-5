const Shape = require('./index');

class Rectangle extends Shape {
  constructor(side) {
    super();
    this.side = side;
  }

  get area() {
    return this.side * this.side;
  }

}


module.exports = Rectangle;
