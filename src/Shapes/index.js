const NOT_IMPL_ERR = 'Not implemented'

class Shape {

  get perimeter() {
    throw new Error(NOT_IMPL_ERR)
  }

}

module.exports = Shape;
